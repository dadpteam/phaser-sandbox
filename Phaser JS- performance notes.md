#Phaser JS: performance notes

This document contains accumulated experience of developers from different sources.
The main purpose of the document is to provide a basic knowledge that a developer needs to consider before start to build a web game app.

#####P2:
* polygon shapes will be more expensive than simple circles - especially huge polygons (keep them simple)
* enabling p2 impactEvents is expensive - use onBeginContact instead
* having a bunchload of revolute constraints seems to be expensive

#####Arcade:
* don't call  game.physics.arcade.collide   20 times in the update loop.. try to do the same thing with 5 calls ;)

#####Common:
* reuse sprites whenever possible - create multiple / kill and revive them (e.g. bullets)
* be careful with the resolution of your game 640x480 will run faster than 1024x768
keep physics simple
* instead of "game.time.now" use "game.time.time" or one of the physicsElapsed values, as "now" can often contain high precision timer values (depending on the browser)
* arcade is faster than p2
* rendering normal text is expensive, using bitmap-fonts seems to be better but is still somehow heavy on mobile
* using tilemaps with more than 2 layers is going to be slow (depending on your device) - try the new tilemap plugin
* huge tilemaps will be slow
* when using webGL use a sprite atlas that contains all your sprites instead of many different image files.
* make use of the particles system - but not to much ^^ (it may be better to use the particlesystem than to create all particles "by hand" but spawning particles will cause a frame drop on slow devices)
* try to keep the update loop simple - do not call a function 60 times a second if not definitely needed
* make sure the garbage collector can free some memory - use **var**  ^^
* make sure the garbage collector doesn't have too much to do - don't create new objects all the time (functions are treated in the same way)  and finding the balance between to much and to little is the trick
* don't use the native webview (especially on older android versions) - use a wrapper (like cocoonjs) or use intel crosswalk (which builds on chromium)
* intel  crosswalk seems to run much better with webgl enabled
* cocoonJS seems to run much better in canvas mode
* do not use splice. If you have only one element to cut off an array, use this function, which is multiple times faster that the build in one, and aside it low GC (the built in is the opposite!): 1-item-splice
* using a lot of tweens in parallel can cause lags
* using Prototype simply slows down things. It's faster to use precached functions.
* do not use classes, no new, no prototype, no this, track length by yourself, use * custom splice or avoid it completely by using double linked lists
* tweens that start on creation of a state will be choppy.. use game.time.events.add(1, fadeIn); to delay the tween (it seems that game.time.events will not start before everything is loaded)
* WebGL stuff runs much faster if you use a single texture atlas for all of your assets. On canvas this doesn't matter so much.
* Lots of small optimisations add up - doing things like avoiding division (i.e. using "*** 0.5**" to half a value instead of "**/ 2**") and rounding down numbers with | 0 in your code can speed it up, especially in big loops or your update/render functions.
* Using the appropriate physics system (and using it appropriately) helps a lot - Arcade is way faster than P2, and if you can get away with doing something with tweens, then that's faster still.
* Try to only update things which need updating. Be as ruthless as you can with regards to cutting out any unnecessary operations. Be especially vigilant with what you put in the update and render loops; calling the same thing over and over when it doesn't change is wasteful, and don't expect Phaser to be intelligent enough to realise nothing has changed - it will in most cases still repeat the same operation; and just because something isn't visually changing doesn't mean there aren't a lot of calculations going on anyway. A common approach (and one Phaser and pixi both use internally) is the idea of 'invalidation'. Objects have a 'dirty' flag, which is set to true whenever the object is changed. Then, on the object's update or render loop, if this flag is seen to be true, the object is updated and the flag is set back to false. This means the object is only updated when it needs to be updated, and if multiple things are changed, they're all updated in the same pass.
* For applying a function every two seconds, you can use a variable in your update function. Let's call it 'timeout':
```javascript
	function update(){
      if (game.time.time> timeout) {
          call_your_function();
          timeout= game.time.time+2000;
      }
      // ... your code
	}
```
Remember to initialize 'timeout'. Check Time class for more properties.
Would it do the trick? This is a way more efficient way of doing it instead of timeouts or intervals, and as you see, changing 2000 for a variable, you can call the function at variable (or random) intervals.
* Never ever use an array with pop() and push() to pool your objects, GC (garbage collector) will punish you for this for sure


* **Particle System: Slow**
iPad Air / iPad Mini, same result: If you use an particle system effect you get lost a lot of FPS.
As more complicated the graphics or amount of particle are the slower the game runs.
* **Bitmaps: Very Slow**
Bitmaps. We are now avoiding the use of any kind of bitmaps. Because indifferent for which kind you use it, it slows everything down.
Backgrounds, Bitmap font, gradients... only one bitmap causes a few fps..
* **WebGL: System crashes**
we also running cocoonJS in Canvas-Mode. With WebGL we had a lot of crashes and rarely total system crashes of iOS 7 & 8. with Black Screen and no reaction of the device for about 5 - 10 Minutes.
* **Tweenings: lagging**
One problem that we were not able to solve is that if you use a few tweenings parallel:
All devices which are not at the hardware level of the iPad Air are lagging for a few fps.


* **Prototype simply slows down things.**
It's 4x faster to use precached functions. Avoiding classes alltogether increase speed even more. I for example do not use classes, no new, no prototype, no this, track length by myself, custom splice or avoid it completely by using double linked lists etc.


* "**prototyping vs precached functions**".
prototype aproach:
```javascript
	classABC = function () {};
    classABC.prototype.myMethod1 = function () {
    	console.log('myMethod');
    }
    classABC.prototype.myMethod2 = function () {
    	console.log('myMethod2');
    }
```
precached functions aproach:
```javascript
	var myMethod1F = function () {
    	console.log('myMethod1');
    };
    var myMethod2F = function () {
    	console.log('myMethod2');
    };
    classABC = function () {
    	return {
        	myMethod1: myMethod1F,
            myMethod2: myMethod2F
        }
    }
```
  * [**Performance test**: Closure vs Prototype vs Static](https://jsperf.com/closure-prototype-static-performance)
  * [To Prototype, or not to Prototype](https://www.reddit.com/r/webdev/comments/3cbz6o/to_prototype_or_not_to_prototype_a_look_at_memory/)


* I used to get 30 fps on an iPhone 4s. Fixed it by checking the device first, put the renderer on WebGL, remove the advanced timing and fps count and BINGO - 60fps:)
Also, after the game ran perfectly on iphone 4s, my Samsung Galaxy S3 Neo (Chrome) started to render the game at 30 fps when using WebGL instead of Canvas(60fps). After 2 weeks of testing and stuff, I have concluded that, for some reason, the android 4.4.2 phones, like Canvas more than WebGL. Newer devices, like Samsung Galaxy S6 and S7, don't care because of the power they provide.
So what you need to do is check the device, if android - Canvas, if iOS - WebGL. That is based on a very simple game, no physics or whatsoever.
